<?php
   
   // If the user is not logged in then do not allow access to this file.
   session_start();
   if (! isset($_SESSION['session'])) {
		echo "You are not logged in!!";
		exit();
   }
   
   $localhostIsUsed = true;
   $dbhost;
   $dbuser;
   $dbpass;
   $dbname;
   
   if ($localhostIsUsed) {
	   $dbhost = "localhost";
	   $dbuser = "root";
	   $dbpass = "";
	   $dbname = "geoshare";
   } else {
	   $dbhost = "127.13.124.2:3306";
	   $dbuser = "admineXQPUfl";
	   $dbpass = "WJHynzRyx4Xe";
	   $dbname = "geoshare";
   }
   
   $tableItems = "items";
   $tableLocations = "locations";
   $tableSharings = "sharings";
   $tableUsers = "users";
   $tableRatings = "ratings";
   
   // Connecting to MySQL Server
   mysql_connect($dbhost, $dbuser, $dbpass);
   
   // Selecting the Database
   mysql_select_db($dbname) or die(mysql_error());
   
   // Getting data from the 'queryType' parameter
   $typeOfQuery = $_GET['queryType'];
   
   // Preventing SQL injection by escaping the user input
   $typeOfQuery = mysql_real_escape_string($typeOfQuery);
   
   $ADD_SHARED_ITEMS = 'add_shared_items';
   $GET_SHARED_ITEMS = 'get_shared_items';
   $GET_CURRENT_USER = 'get_current_user';
   $GET_CURRENT_USER_LOCATIONS = 'get_current_user_locations';
   $GET_CURRENT_USER_SHARED_ITEMS = 'get_current_user_shared_items';
   $ADD_LOCATION = 'add_location';
   $REMOVE_LOCATION = 'remove_location';
   $BOOK_ITEM = 'book_item';
   $CANCEL_BOOKED_ITEM = 'cancel_booked_item';
   $REMOVE_SHARED_ITEM = 'remove_shared_item';
   $EDIT_ITEM = 'edit_item';
   $RECEIVED_ITEM = 'received_item';
   $RATE_ITEM = 'rate_item';
   $EDIT_PERSONAL_INFO = 'edit_personal_info';
   
   if ($typeOfQuery == 'add_shared_items') {
		
		$tableItems = "items";
		$tableSharings = "sharings";
		
		// Getting data from the parameters
		$sharedItemTitleParam = $_GET['sharedItemTitle'];
		$sharedItemCategoryParam = $_GET['sharedItemCategory'];
		$sharedItemDescriptionParam = $_GET['sharedItemDescription'];
		//$sharedItemTradeTypeParam = $_GET['sharedItemTradeType'];
		$sharedItemLocationIdParam = $_GET['sharedItemLocationId'];
		$sharedItemGiverIdParam = $_GET['sharedItemGiverId'];
		
		// Preventing SQL injection by escaping the user input
		$sharedItemTitleParam = mysql_real_escape_string($sharedItemTitleParam);
		$sharedItemCategoryParam = mysql_real_escape_string($sharedItemCategoryParam);
		$sharedItemDescriptionParam = mysql_real_escape_string($sharedItemDescriptionParam);
		//$sharedItemTradeTypeParam = mysql_real_escape_string($sharedItemTradeTypeParam);
		$sharedItemLocationIdParam = mysql_real_escape_string($sharedItemLocationIdParam);
		$sharedItemGiverIdParam = mysql_real_escape_string($sharedItemGiverIdParam);
		
		$query = "INSERT INTO " . $tableItems . "(name, category, description) VALUES ('" . $sharedItemTitleParam . "', '" . $sharedItemCategoryParam . "', '" . $sharedItemDescriptionParam . "')";
		
		// Executing the Query
		mysql_query($query) or die(mysql_error());
		
		$inserted_item_id = mysql_insert_id();
		
		//$query = "INSERT INTO " . $tableSharings . "(trade_type, item_id_frkey, location_id_frkey, giver_id) VALUES ('" . $sharedItemTradeTypeParam . "', '" . $inserted_item_id . "', '" . $sharedItemLocationIdParam . "', '" . $sharedItemGiverIdParam . "')";
		$query = "INSERT INTO " . $tableSharings . "(item_id_frkey, location_id_frkey, giver_id) VALUES ('" . $inserted_item_id . "', '" . $sharedItemLocationIdParam . "', '" . $sharedItemGiverIdParam . "')";
		
		$qry_result = mysql_query($query) or die(mysql_error());
		
		echo $qry_result;
   }
   
   else if ($typeOfQuery == $GET_SHARED_ITEMS) {
   
   $table = "locations";
   
   // Getting data from the parameters
   $itemKeywordField = $_GET['fieldItemKeyword'];
   $itemCategoryField = $_GET['fieldItemCategory'];
   //$itemTradeTypeField = $_GET['fieldItemTradeType'];
   $giverIdField = $_GET['fieldGiverId'];
   $onlyShowItemsSharedOrBookedField = $_GET['fieldOnlyShowItemsSharedOrBooked'];
   
   // Preventing SQL injection by escaping the user input
   $itemKeywordField = mysql_real_escape_string($itemKeywordField);
   $itemCategoryField = mysql_real_escape_string($itemCategoryField);
   //$itemTradeTypeField = mysql_real_escape_string($itemTradeTypeField);
   $giverIdField = mysql_real_escape_string($giverIdField);
   $onlyShowItemsSharedOrBookedField = mysql_real_escape_string($onlyShowItemsSharedOrBookedField);
   
   // Building the MySQL Query
	//$query = "SELECT sharings.sharing_id, sharings.trade_type, sharings.trade_status, sharings.giver_id, sharings.taker_id, sharings.timestamp, items.item_id, items.name, items.category, items.description, locations.location_id, locations.address, locations.lat, locations.lng, users.user_id, users.username, users.firstname, users.surname, users.email, users.phone FROM sharings, items, locations, users WHERE item_id_frkey = items.item_id AND location_id_frkey = locations.location_id AND giver_id = users.user_id";
	//$query = "SELECT sharings.sharing_id, sharings.trade_type, sharings.trade_status, sharings.giver_id, sharings.taker_id, sharings.rated, sharings.timestamp, items.item_id, items.name, items.category, items.description, locations.location_id, locations.address, locations.lat, locations.lng, users.user_id, users.username, users.firstname, users.surname, users.email, users.phone ";
	$query = "SELECT sharings.sharing_id, sharings.trade_status, sharings.giver_id, sharings.taker_id, sharings.rated, sharings.timestamp, items.item_id, items.name, items.category, items.description, locations.location_id, locations.address, locations.lat, locations.lng, users.user_id, users.username, users.firstname, users.surname, users.email, users.phone ";
	$query .= ", (SELECT COUNT(star_value) count FROM ratings WHERE ratings.rated_user_id = sharings.giver_id) AS rating_count ";
	$query .= ", (SELECT AVG(star_value) average FROM ratings WHERE ratings.rated_user_id = sharings.giver_id) AS rating_average ";
	$query .= ", (SELECT SUM(star_value) sum FROM ratings WHERE ratings.rated_user_id = sharings.giver_id) AS rating_sum ";
	// The following two lines can be used to retrieve other data e.g. names and concatenate them together:
	//$query .= ", (SELECT CONCAT(firstname, ' ', surname) name FROM users WHERE giver_id = users.user_id) AS giver_full_name ";
	//$query .= ", (SELECT CONCAT(firstname, ' ', surname) name FROM users WHERE taker_id = users.user_id) AS taker_full_name ";
	$query .= " FROM sharings, items, locations, users WHERE item_id_frkey = items.item_id AND location_id_frkey = locations.location_id AND giver_id = users.user_id";
	
	// Only get the shared items by the current user
	if ($onlyShowItemsSharedOrBookedField == 'shared') {
		$query .= " AND giver_id = " . $giverIdField;
	}
	// Only get the booked items by the current user
	else if ($onlyShowItemsSharedOrBookedField == 'booked') {
		$query .= " AND taker_id = " . $giverIdField;
	}
	// Get all items shared by all users except the ones shared by the current user
	else {
		$query .= " AND giver_id != " . $giverIdField . " AND taker_id = '0'";
		
		// Filter the retrieved items by the search filter fields
		if ($itemKeywordField != '')
			$query .= " AND ( items.name LIKE '%" . $itemKeywordField .  "%' OR items.description LIKE '%" . $itemKeywordField . "%' )";
		if ($itemCategoryField != 'All')
			$query .= " AND ( items.category LIKE '" . $itemCategoryField . "' )";
		//if ($itemTradeTypeField != 'All')
		//	$query .= " AND ( sharings.trade_type LIKE '" . $itemTradeTypeField . "' )";
	}
   
   // Executing the Query
   $qry_result = mysql_query($query) or die(mysql_error());
   
   // Building a result string out of the retrieved data
   //$display_string = '{"'.$table.'":[';
   $sharings_string = '"sharings":[';
   $ratings_string = '"ratings":[';
   $items_string = '"items":[';
   $locations_string = '"locations":[';
   $users_string = '"users":[';
   
   // Inserting a new row in the table for each retrieved record
   
   $numOfRows = mysql_num_rows($qry_result);
   $counter = 0;
   
   while($row = mysql_fetch_array($qry_result)) {
      
	  $sharings_string .= '{"sharing_id":'.$row['sharing_id'].', ';
      //$sharings_string .= '"trade_type":"'.$row['trade_type'].'", ';
      $sharings_string .= '"trade_status":"'.$row['trade_status'].'", ';
      $sharings_string .= '"giver_id":'.$row['giver_id'].', ';
      $sharings_string .= '"taker_id":'.$row['taker_id'].', ';
      $sharings_string .= '"rated":"'.$row['rated'].'", ';
      $sharings_string .= '"timestamp":"'.$row['timestamp'].'"}';
	  
      $ratings_string .= '{"rating_average":"'.$row['rating_average'].'", ';
      $ratings_string .= '"rating_count":"'.$row['rating_count'].'", ';
      $ratings_string .= '"rating_sum":"'.$row['rating_sum'].'"}';
	  
      $items_string .= '{"item_id":'.$row['item_id'].', ';
      $items_string .= '"name":"'.$row['name'].'", ';
      $items_string .= '"category":"'.$row['category'].'", ';
      $items_string .= '"description":"'.$row['description'].'"}';
	  
      $locations_string .= '{"location_id":'.$row['location_id'].', ';
      $locations_string .= '"address":"'.$row['address'].'", ';
      $locations_string .= '"lat":'.$row['lat'].', ';
      $locations_string .= '"lng":'.$row['lng'].'}';
	  
      $users_string .= '{"user_id":'.$row['user_id'].', ';
      $users_string .= '"username":"'.$row['username'].'", ';
      $users_string .= '"firstname":"'.$row['firstname'].'", ';
      $users_string .= '"surname":"'.$row['surname'].'", ';
      $users_string .= '"email":"'.$row['email'].'", ';
      $users_string .= '"phone":'.$row['phone'].'}';
	  
	  $counter++;
	  // If not the last row, then add a comma separator
	  if ($counter < $numOfRows) {
		$sharings_string .= ', ';
		$ratings_string .= ', ';
		$items_string .= ', ';
		$locations_string .= ', ';
		$users_string .= ', ';
	  }
   }
   //echo "The Built Query is: " . $query . "<br />";
   
   $sharings_string .= '], ';
   $ratings_string .= '], ';
   $items_string .= '], ';
   $locations_string .= '], ';
   $users_string .= ']';
   
   $all_strings = '{' . $sharings_string . $ratings_string . $items_string . $locations_string .$users_string . '}';
   //echo "The Retrieved Data is: " . $display_string . "<br />";
   
   echo json_encode($all_strings);
   }
   
	else if ($typeOfQuery == $GET_CURRENT_USER) {
		
		// Getting data from the parameters
		$userIdField = $_GET['fieldUserId'];
		
		// Preventing SQL injection by escaping the user input
		$userIdField = mysql_real_escape_string($userIdField);
		
		// Building the MySQL Query
		$query = "SELECT user_id, username, firstname, surname, email, phone FROM " . $tableUsers . " WHERE user_id = " . $userIdField;
		
		// Executing the Query
		$qry_result = mysql_query($query) or die(mysql_error());
		
		$result = mysql_fetch_assoc($qry_result);
		
		$json_result = '{"users":[{"user_id":';
		$json_result .= $result['user_id'] . ', "username":"';
		$json_result .= $result['username'] . '", "firstname":"';
		$json_result .= $result['firstname'] . '", "surname":"';
		$json_result .= $result['surname'] . '", "email":"';
		$json_result .= $result['email'] . '", "phone":"';
		$json_result .= $result['phone'] . '"}]}';
		
		echo json_encode($json_result);
	}
	else if ($typeOfQuery == $GET_CURRENT_USER_LOCATIONS) {
		
		// Getting data from the parameters
		$userIdField = $_GET['fieldUserId'];
		
		// Preventing SQL injection by escaping the user input
		$userIdField = mysql_real_escape_string($userIdField);
		
		// Building the MySQL Query
		//$query = "SELECT * FROM locations WHERE location_id IN (SELECT location_id_frkey from sharings WHERE giver_id = 5)";
		$query = "SELECT location_id, address, lat, lng FROM locations WHERE user_id_frkey = " . $userIdField . " AND is_used LIKE 'YES'";
		
		// Executing the Query
		$qry_result = mysql_query($query) or die(mysql_error());
		
		// Building a result string out of the retrieved data
		$locations_string = '"locations":[';
		
		$numOfRows = mysql_num_rows($qry_result);
		$counter = 0;
		
		while($row = mysql_fetch_array($qry_result)) {
		  $locations_string .= '{"location_id":'.$row['location_id'].', ';
		  $locations_string .= '"address":"'.$row['address'].'", ';
		  $locations_string .= '"lat":'.$row['lat'].', ';
		  $locations_string .= '"lng":'.$row['lng'].'}';
		  $counter++;
		  // If not the last row, then add a comma separator
		  if ($counter < $numOfRows)
			$locations_string .= ', ';
		}
		$locations_string .= ']';
		$all_strings = '{' . $locations_string . '}';
		
		echo json_encode($all_strings);
	}
	else if ($typeOfQuery == $GET_CURRENT_USER_SHARED_ITEMS) {
		
		echo json_encode('{"sharings":[{}]}');
	}
	else if ($typeOfQuery == $ADD_LOCATION) {
		
		// Getting data from the parameters
		$userIdField = $_GET['fieldUserId'];
		$locationAddressField = $_GET['fieldAddress'];
		$locationLatField = $_GET['fieldLocationLat'];
		$locationLngField = $_GET['fieldLocationLng'];
		
		// Preventing SQL injection by escaping the user input
		$userIdField = mysql_real_escape_string($userIdField);
		$locationAddressField = mysql_real_escape_string($locationAddressField);
		$locationLatField = mysql_real_escape_string($locationLatField);
		$locationLngField = mysql_real_escape_string($locationLngField);
		
		// Building the MySQL Query
		$query = "INSERT INTO " . $tableLocations . "(address, lat, lng, user_id_frkey) VALUES ('" . $locationAddressField . "', '" . $locationLatField . "', '" . $locationLngField . "', '" . $userIdField . "')";
		
		// Executing the Query
		$qry_result = mysql_query($query) or die(mysql_error());
		
		echo json_encode($qry_result);
	}
	else if ($typeOfQuery == $REMOVE_LOCATION) {
		
		// Getting data from the parameters
		$userIdField = $_GET['fieldUserId'];
		$fieldLocationId = $_GET['fieldLocationId'];
		
		// Preventing SQL injection by escaping the user input
		$userIdField = mysql_real_escape_string($userIdField);
		$fieldLocationId = mysql_real_escape_string($fieldLocationId);
		
		// Building the MySQL Query
		$query = "SELECT COUNT(*) AS count FROM sharings WHERE giver_id = " . $userIdField . " AND location_id_frkey = " . $fieldLocationId . " AND trade_status LIKE 'INCOMPLETE'";
		
		// Executing the Query
		$qry_result = mysql_query($query) or die(mysql_error());
		
		$result = mysql_fetch_assoc($qry_result);
		
		if ($result['count'] == 0) {
			$query = "UPDATE " . $tableLocations . " SET is_used = 'NO' WHERE location_id = " . $fieldLocationId;
			$qry_result = mysql_query($query) or die(mysql_error());
		} else {
			$qry_result = false;
		}
		
		echo json_encode($qry_result);
	}
	else if ($typeOfQuery == $BOOK_ITEM) {
		
		// Getting data from the parameters
		$userIdField = $_GET['fieldUserId'];
		$sharingIdField = $_GET['fieldSharingId'];
		
		// Preventing SQL injection by escaping the user input
		$userIdField = mysql_real_escape_string($userIdField);
		$sharingIdField = mysql_real_escape_string($sharingIdField);
		
		// Building the MySQL Query
		$query = "UPDATE sharings SET taker_id = " . $userIdField . " WHERE sharing_id =" . $sharingIdField;
		
		// Executing the Query
		$qry_result = mysql_query($query) or die(mysql_error());
		
		echo json_encode($qry_result);
	}
	else if ($typeOfQuery == $CANCEL_BOOKED_ITEM) {
		
		// Getting data from the parameters
		$sharingIdField = $_GET['fieldSharingId'];
		
		// Preventing SQL injection by escaping the user input
		$sharingIdField = mysql_real_escape_string($sharingIdField);
		
		$zero = 0;
		// Building the MySQL Query
		$query = "UPDATE sharings SET taker_id = " . $zero . " WHERE sharing_id =" . $sharingIdField;
		
		// Executing the Query
		$qry_result = mysql_query($query) or die(mysql_error());
		
		echo json_encode($qry_result);
	}
	else if ($typeOfQuery == $REMOVE_SHARED_ITEM) {
		
		// Getting data from the parameters
		$sharingIdField = $_GET['fieldSharingId'];
		$itemIdField = $_GET['fieldItemId'];
		
		// Preventing SQL injection by escaping the user input
		$sharingIdField = mysql_real_escape_string($sharingIdField);
		$itemIdField = mysql_real_escape_string($itemIdField);
		
		// Building the MySQL Query
		$query = "DELETE from sharings WHERE sharing_id = " . $sharingIdField;
		
		// Executing the Query
		$qry_result = mysql_query($query) or die(mysql_error());
		
		// Building the MySQL Query
		$query = "DELETE from items WHERE item_id = " . $itemIdField;
		
		// Executing the Query
		$qry_result = mysql_query($query) or die(mysql_error());
		
		echo json_encode($qry_result);
	}
	else if ($typeOfQuery == $EDIT_ITEM) {
		
		// Getting data from the parameters
		$editItemTitleParam = $_GET['editItemTitle'];
		$editItemCategoryParam = $_GET['editItemCategory'];
		$editItemDescriptionParam = $_GET['editItemDescription'];
		//$editItemTradeTypeParam = $_GET['editItemTradeType'];
		$editItemLocationIdParam = $_GET['editItemLocationId'];
		
		$sharingIdField = $_GET['fieldSharingId'];
		$itemIdField = $_GET['fieldItemId'];
		
		// Preventing SQL injection by escaping the user input
		$editItemTitleParam = mysql_real_escape_string($editItemTitleParam);
		$editItemCategoryParam = mysql_real_escape_string($editItemCategoryParam);
		$editItemDescriptionParam = mysql_real_escape_string($editItemDescriptionParam);
		//$editItemTradeTypeParam = mysql_real_escape_string($editItemTradeTypeParam);
		$editItemLocationIdParam = mysql_real_escape_string($editItemLocationIdParam);
		
		$sharingIdField = mysql_real_escape_string($sharingIdField);
		$itemIdField = mysql_real_escape_string($itemIdField);
		
		// Building the MySQL Query
		$query = "UPDATE " . $tableItems . " SET name = '" . $editItemTitleParam . "', category = '" . $editItemCategoryParam . "', description = '" . $editItemDescriptionParam . "' WHERE item_id = " . $itemIdField;
		
		// Executing the Query
		$qry_result = mysql_query($query) or die(mysql_error());
		
		// Building the MySQL Query
		//$query = "UPDATE " . $tableSharings . " SET trade_type = '" . $editItemTradeTypeParam . "', location_id_frkey = " . $editItemLocationIdParam . " WHERE sharing_id = " . $sharingIdField;
		$query = "UPDATE " . $tableSharings . " SET location_id_frkey = " . $editItemLocationIdParam . " WHERE sharing_id = " . $sharingIdField;
		
		// Executing the Query
		$qry_result = mysql_query($query) or die(mysql_error());
		
		echo json_encode($qry_result);
	}
	else if ($typeOfQuery == $RECEIVED_ITEM) {
		
		// Getting data from the parameters
		$sharingIdField = $_GET['fieldSharingId'];
		
		// Preventing SQL injection by escaping the user input
		$sharingIdField = mysql_real_escape_string($sharingIdField);
		
		// Building the MySQL Query
		$query = "UPDATE " . $tableSharings . " SET trade_status = 'COMPLETE' WHERE sharing_id = " . $sharingIdField;
		
		// Executing the Query
		$qry_result = mysql_query($query) or die(mysql_error());
		
		echo json_encode($qry_result);
	}
	else if ($typeOfQuery == $RATE_ITEM) {
		
		// Getting data from the parameters
		$ratingStarField = $_GET['fieldRatingStar'];
		$userToRateField = $_GET['fieldUserToRate'];
		$sharingIdField = $_GET['fieldSharingId'];
		
		// Preventing SQL injection by escaping the user input
		$ratingStarField = mysql_real_escape_string($ratingStarField);
		$userToRateField = mysql_real_escape_string($userToRateField);
		$sharingIdField = mysql_real_escape_string($sharingIdField);
		
		// Building the MySQL Query
		$query = "INSERT into " . $tableRatings . " (star_value, rated_user_id) VALUES (" . $ratingStarField . ", " . $userToRateField . ")";
		
		// Executing the Query
		$qry_result = mysql_query($query) or die(mysql_error());
		
		// Building the MySQL Query
		$query = "UPDATE " . $tableSharings . " SET rated = 'YES' WHERE sharing_id = " . $sharingIdField;
		
		// Executing the Query
		$qry_result = mysql_query($query) or die(mysql_error());
		
		echo json_encode($qry_result);
	}
	else if ($typeOfQuery == $EDIT_PERSONAL_INFO) {
		
		// Getting data from the parameters
		$firstnameField = $_GET['fieldFirstname'];
		$surnameField = $_GET['fieldSurname'];
		$emailField = $_GET['fieldEmail'];
		$phoneField = $_GET['fieldPhone'];
		$userIdField = $_GET['fieldUserId'];
		
		// Preventing SQL injection by escaping the user input
		$firstnameField = mysql_real_escape_string($firstnameField);
		$surnameField = mysql_real_escape_string($surnameField);
		$emailField = mysql_real_escape_string($emailField);
		$phoneField = mysql_real_escape_string($phoneField);
		$userIdField = mysql_real_escape_string($userIdField);
		
		// Building the MySQL Query
		$query = "UPDATE " . $tableUsers . " SET firstname = '" . $firstnameField . "', surname = '" . $surnameField . "', email = '" . $emailField . "', phone = '" . $phoneField . "' WHERE user_id = " . $userIdField;
		
		// Executing the Query
		$qry_result = mysql_query($query) or die(mysql_error());
		
		echo json_encode($qry_result);
	}
?>