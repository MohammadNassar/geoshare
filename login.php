<?php
	session_start();
	
	if (isset($_SESSION['session'])) {
		echo "You are logged in.";
		exit();
	}
?>

<html>

	<head>
		<style type="text/css">
		</style>
		
		<meta name="viewport" content="width=device-width, initial-scale=1">
		
		<link type="text/css" href="sidebar.css" rel="stylesheet"></link>
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
		
		<script type="text/javascript" src="ajaxwork.js"></script>
		
		<title>GEOSHARE - Login</title>
		
	</head>

	<body>
	<div class="container">
		<form role="form" action="login-verify.php" method="post">
		
		<br>
		
		<div role="form-group">
			<label for="username">Username:</label>
			<input type="text" name="username" class="form-control" placeholder="Username" /> <br />
			
			<label for="password">Password:</label>
			<input type="password" name="password" class="form-control" placeholder="Password" /> <br />
		</div>
		
		<input type="submit" name="submit" value="Login" class="btn btn-primary form-control" /><br />
		
		<input name="accessType" value="login" style="visibility: hidden; display: none;" />
		
		</form>
		
		<center><button onclick="location.href='register.php'" type="button" class="btn btn-link btn-lg">Register</button></center>
	</div>

	</body>
</html>