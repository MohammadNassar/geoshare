<!DOCTYPE html>
<?php

	error_reporting(0);
	
	session_start();
	
	if (!$_SESSION['session']) {
		header('Location: login.php');
	}

?>
<html>
  <head>
    <style type="text/css">
      html, body { height: 100%; margin: 0; padding: 0; }
      #container { height: 100%; }
      #map { height: 96%; }
	  .tab-content { height: 60%; overflow-y: scroll; }
	  #list-for-markers { height: 30%; overflow-y: scroll; }
	  #userinfo { text-align: center; font-weight: bold; margin-top: 5px; }
    </style>
	
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<link type="text/css" href="sidebar.css" rel="stylesheet"></link>
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
	
	<script type="text/javascript" src="ajaxwork.js"></script>
	
	<title>GEOSHARE</title>
	
  </head>
  <body onload='callOnLoad();'>
	
	<div id="container">
	
		<div id="response">Response</div>
		
		<div id="current-user-info">Current User Info</div>
		<div id="current-user-stored-locations">Current User Stored Locations</div>
		<div id="current-user-shared-items">Current User Shared Items</div>
		
		<div id="current-latitude"></div>
		<div id="current-longitude"></div>
		
		<div id="generic-response">Generic Response</div>
		
		<nav>
			<div class="btn-group btn-group-justified" role="group" aria-label="Main Options">
				
				<div class="btn-group" role="group">
					<button type="button" class="btn btn-default" id="menu-toggle">
						<span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span> <b>Menu </b>
					</button>
				</div>
				
				<div class="btn-group" role="group">
					<button type="button" class="btn btn-default" onclick="location.href='logout.php';">
						<span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> <b>Log Out </b>
					</button>
				</div>
				
			</div>
		</nav>
		
		<div id="sidebar-container">
			
			<div class="tab-content">
				
				<ul class="nav nav-tabs nav-justified">
					<li class="active" id="tab1Triggerer">
						<a data-toggle="tab" href="#tab1"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Search</a>
					</li>
					<li id="tab2Triggerer">
						<a data-toggle="tab" href="#tab2"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Profile</a>
					</li>
				</ul>
				
				<div class="tab-pane fade in active" id="tab1">
					
					<div class="navbar-form navbar-left" role="search">
						<div class="form-group">
							<input type="text" class="form-control" placeholder="Item Keyword" id="search-item-keyword">
							<br><br>
							
							<label for="searchItemLocationRange">Location Range</label><br>
							<select class="form-control" id="search-item-location-range">
								<option>All</option>
								<option>1</option>
								<option>2</option>
								<option>3</option>
								<option>4</option>
								<option>5</option>
								<option>10</option>
								<option>20</option>
								<option>30</option>
							</select>
							<br><br>
							
							<label for="searchItemCategory">Item Category</label>
							<select class="form-control" id="search-item-category">
								<option>All</option>
								<option>Baby and Nursery</option>
								<option>Books</option>
								<option>Clothing</option>
								<option>Electronics and PCs</option>
								<option>Furniture</option>
								<option>Health and Beauty</option>
								<option>Home and Garden</option>
								<option>Jewellery and Watches</option>
								<option>Motors</option>
								<option>Pets</option>
								<option>Sports and Leisure</option>
								<option>Toys</option>
							</select>
							<br><br>
							
							<!--
							<label for="search-item-trade-type">Trade Type</label><br>
							<select class="form-control" id="search-item-trade-type">
								<option>All</option>
								<option>Bartering</option>
								<option>No Exchange</option>
							</select>
							<br><br>
							-->
							
							<button type="submit" class="btn btn-default" onclick="setShowItemsSharedOrBooked('available_shares'); ajaxFunction('get_shared_items'); toggleMenu();">
								<span class="glyphicon glyphicon-search" aria-hidden="true"></span> Search
							</button>
							<br><br>
						</div>
					</div>
					
				</div>
				
				<div id="tab2" class="tab-pane fade">
					
					<div id="userinfo"></div>
					
					<ul class="sidebar-nav">
						<li><a href="#" data-toggle="modal" data-target="#shareItemModal" id="share-item-modal-triggerer" onclick="openShareItemModalOrNot()">
							<span class="glyphicon glyphicon-share" aria-hidden="true"></span> Share an Item</a>
						</li>
						<li>
							<a href="#" data-toggle="modal" data-target="#" onclick="setShowItemsSharedOrBooked('shared');ajaxFunction('get_shared_items'); toggleMenu();"><span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> Show My Shared Items</a>
						</li>
						<li>
							<a href="#" data-toggle="modal" data-target="#" onclick="setShowItemsSharedOrBooked('booked');ajaxFunction('get_shared_items'); toggleMenu();"><span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span> Show My Booked Items</a>
						</li>
						<li>
							<a href="#" data-toggle="modal" data-target="#saveCurrentUserLocationModal"><span class="glyphicon glyphicon-save" aria-hidden="true"></span> Save My Current Location</a>
						</li>
						<li>
							<a href="#" data-toggle="modal" data-target="#currentUserLocationsModal" id="current-user-locations-modal-triggerer" onclick="openShareItemModalOrNot()"><span class="glyphicon glyphicon-saved" aria-hidden="true"></span> My Stored Locations</a>
						</li>
						<li>
							<a href="#" data-toggle="modal" data-target="#" onclick="getCurrentLocation(); toggleMenu();"><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> View My Location</a>
						</li>
						<li><a href="#" data-toggle="modal" data-target="#editPersonalInfoModal" onclick="populateFieldsForEditPersonalInfoModal();">
							<span class="glyphicon glyphicon-user" aria-hidden="true"></span> Edit My Personal Info</a>
						</li>
					</ul>
					
				</div>
				
			</div>
			
			<hr>
			
			<div class="list-group" id="list-for-markers">
			</div>
			
		</div>
		
		<!-- Share Item Modal -->
		<div id="shareItemModal" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<form>
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h3 class="modal-title"><span class="glyphicon glyphicon-share" aria-hidden="true"></span> Share an Item</h3>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<label for="shareItemTitle">Item Title</label>
							<input type="text" class="form-control" id="shared-item-title">
							<br>
							
							<label for="shareItemCategory">Item Category</label>
							<select class="form-control" id="share-item-category">
								<option>Baby and Nursery</option>
								<option>Books</option>
								<option>Clothing</option>
								<option>Electronics and PCs</option>
								<option>Furniture</option>
								<option>Health and Beauty</option>
								<option>Home and Garden</option>
								<option>Jewellery and Watches</option>
								<option>Motors</option>
								<option>Pets</option>
								<option>Sports and Leisure</option>
								<option>Toys</option>
							</select>
							<br>
							
							<label for="shareItemDescription">Item Description</label>
							<textarea class="form-control" id="shared-item-description" rows="5"></textarea>
							<br>
							
							<!--
							<label for="shareItemTradeType">Trade Type</label>
							<select class="form-control" id="share-item-trade-type">
								<option>Bartering</option>
								<option>No Exchange</option>
							</select>
							<br>
							-->
							
							<div id="share-item-location-container"></div>
						</div>
					</div>
					<div class="modal-footer">
						<div class="progress" id="shared-item-progress-bar" style="visibility: hidden; display: none;">
							<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
								<span class="sr-only">In Progress</span>
							</div>
						</div>
						
						<button type="reset" class="btn btn-warning" id="shared-item-clear">Clear</button>
						
						<button onclick="ajaxFunction('add_shared_items');" type="button" class="btn btn-primary" data-dismiss="modal" id="shared-item-submit">Submit</button>
					</div>
					</form>
				</div>
			</div>
		</div>
		<!-- END OF: Share Item Modal -->
		
		<!-- Save Current User Location Modal -->
		<div id="saveCurrentUserLocationModal" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<form>
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h3 class="modal-title"><span class="glyphicon glyphicon-save" aria-hidden="true"></span> Save My Current Locations</h3>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<label for="shareItemTitle">Address Name</label>
							<input type="text" class="form-control" id="save-current-user-location-address">
						</div>
						
						<div id="current-user-location-add" style="visibility: hidden; display: none;"></div>
					</div>
					<div class="modal-footer">
						<div class="progress" id="save-current-user-location-progress-bar" style="visibility: hidden; display: none;">
							<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
								<span class="sr-only">In Progress</span>
							</div>
						</div>
						
						<button onclick="ajaxFunction('add_location');" type="button" class="btn btn-primary" data-dismiss="modal" id="save-current-user-location-submit">Submit</button>
					</div>
					</form>
				</div>
			</div>
		</div>
		<!-- END OF: Save Current User Location Modal -->
		
		<!-- Current User Locations Modal (Allowing Removal of Locations) -->
		<div id="currentUserLocationsModal" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<form>
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h3 class="modal-title"><span class="glyphicon glyphicon-saved" aria-hidden="true"></span> My Stored Locations</h3>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<div id="current-user-locations-container"></div>
						</div>
						
						<div id="current-user-locations-remove"></div>
					</div>
					<div class="modal-footer">
						<div class="progress" id="current-user-locations-progress-bar" style="visibility: hidden; display: none;">
							<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
								<span class="sr-only">In Progress</span>
							</div>
						</div>
						
						<button onclick="ajaxFunction('remove_location');" type="button" class="btn btn-danger"id="current-user-locations-remove">Remove</button>
					</div>
					</form>
				</div>
			</div>
		</div>
		<!-- END OF: Current User Locations Modal (Allowing Removal of Locations) -->
		
		<!-- Edit Item Modal -->
		<div id="editItemModal" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<form>
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h3 class="modal-title"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit Item Details</h3>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<label for="editItemTitle">Item Title</label>
							<input type="text" class="form-control" id="edit-item-title">
							<br>
							
							<label for="editItemCategory">Item Category</label>
							<select class="form-control" id="edit-item-category">
								<option value="Baby and Nursery">Baby and Nursery</option>
								<option value="Books">Books</option>
								<option value="Clothing">Clothing</option>
								<option value="Electronics and PCs">Electronics and PCs</option>
								<option value="Furniture">Furniture</option>
								<option value="Health and Beauty">Health and Beauty</option>
								<option value="Home and Garden">Home and Garden</option>
								<option value="Jewellery and Watches">Jewellery and Watches</option>
								<option value="Motors">Motors</option>
								<option value="Pets">Pets</option>
								<option value="Sports and Leisure">Sports and Leisure</option>
								<option value="Toys">Toys</option>
							</select>
							<br>
							
							<label for="editItemDescription">Item Description</label>
							<textarea class="form-control" id="edit-item-description" rows="5"></textarea>
							<br>
							
							<!--
							<label for="editItemTradeType">Trade Type</label>
							<select class="form-control" id="edit-item-trade-type">
								<option value="Bartering">Bartering</option>
								<option value="No Exchange">No Exchange</option>
							</select>
							<br>
							-->
							
							<div id="edit-item-location-container"></div>
						</div>
					</div>
					<div class="modal-footer">
						<div class="progress" id="edit-item-progress-bar" style="visibility: hidden; display: none;">
							<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
								<span class="sr-only">In Progress</span>
							</div>
						</div>
						
						<button type="reset" class="btn btn-warning" id="edit-item-clear">Clear</button>
						
						<button onclick="ajaxFunction('edit_item');" type="button" class="btn btn-primary" data-dismiss="modal" id="edit-item-submit">Submit</button>
					</div>
					</form>
				</div>
			</div>
		</div>
		<!-- END OF: Edit Item Modal -->
		
		<!-- Edit Personal Info Modal -->
		<div id="editPersonalInfoModal" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<form>
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h3 class="modal-title"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Edit My Personal Info</h3>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<label for="firstname-text">First Name:</label>
							<input type="text" class="form-control" id="firstname-text">
							<br>
							
							<label for="surname-text">Surname:</label>
							<input type="text" class="form-control" id="surname-text">
							<br>
							
							<label for="email-text">Email:</label>
							<input type="email" class="form-control" id="email-text">
							<br>
							
							<label for="phone-text">Phone:</label>
							<input type="number" class="form-control" id="phone-text">
							<br>
						</div>
					</div>
					<div class="modal-footer">
						<div class="progress" id="edit-personal-info-progress-bar" style="visibility: hidden; display: none;">
							<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
								<span class="sr-only">In Progress</span>
							</div>
						</div>
						
						<button type="reset" class="btn btn-warning" id="edit-personal-info-clear">Clear</button>
						
						<button onclick="ajaxFunction('edit_personal_info');" type="button" class="btn btn-primary" id="edit-personal-info-submit">Submit</button>
					</div>
					</form>
				</div>
			</div>
		</div>
		<!-- END OF: Edit Personal Info Modal -->
		
		<div id="map"></div>
		
    <script type="text/javascript">
		
		function callOnLoad() {
			storeCurrentLatAndLng();
			ajaxFunction("get_shared_items");
			ajaxFunction("get_current_user");
			ajaxFunction("get_current_user_locations");
		}
		
		$("#menu-toggle").click(function (e) {
			e.preventDefault();
			toggleMenu();
		});
		
		function toggleMenu() {
			$("#container").toggleClass("menu-displayed");
		}
		
		function toggleMenuOrNot() {
			if(window.innerWidth < 500) {
				toggleMenu();
			}
		}
		
		function openShareItemModalOrNot() {
			var numberOfStoredLocations = currentUserLocations.locations.length;
			
			var shareItemModalTriggererDiv = document.getElementById('share-item-modal-triggerer');
			var currentUserLocationsModalTriggererDiv = document.getElementById('current-user-locations-modal-triggerer');
			
			// If the current user has no stored locations, then do not display the 'Share Item Modal'
			if (numberOfStoredLocations == 0) {
				alert('You do not have any locations stored!\nPlease store your location first!');
				shareItemModalTriggererDiv.setAttribute('data-toggle', ' ');
				currentUserLocationsModalTriggererDiv.setAttribute('data-toggle', ' ');
			} else {
				shareItemModalTriggererDiv.setAttribute('data-toggle', 'modal');
				currentUserLocationsModalTriggererDiv.setAttribute('data-toggle', 'modal');
			}
		}
		
		function initMap() {
		  
		  //sampleInitMap();
		  //getCurrentLocation();
		  //storeCurrentLatAndLng();
		  //getMarkers();
		  //addMarkers();
		}
		
		function getCurrentLocation() {
		  
		  var map = new google.maps.Map(document.getElementById('map'), {
			  zoom: 15
			});
			
		  // --- Getting user's current location.
		  var infoWindow = new google.maps.InfoWindow({map: map});
		
		  // Try HTML5 geolocation.
		  if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(function(position) {
			  var pos = {
				lat: position.coords.latitude,
				lng: position.coords.longitude
			  };

			  infoWindow.setPosition(pos);
			  infoWindow.setContent('You\'re here');
			  map.setCenter(pos);
			}, function() {
			  handleLocationError(true, infoWindow, map.getCenter());
			});
		  } else {
			// Browser doesn't support Geolocation
			handleLocationError(false, infoWindow, map.getCenter());
		  }
		  // --- END OF Getting user's current location.
		  hideListForMarkers();
		}
		
		function storeCurrentLatAndLng() {
			if (navigator.geolocation) { // Try HTML5 geolocation.
				navigator.geolocation.getCurrentPosition(function(position) {
					document.getElementById('current-latitude').innerHTML = position.coords.latitude;
					document.getElementById('current-longitude').innerHTML = position.coords.longitude;
				});
			} else {console.log('This browser does not support the Geolocation functionality !!');}
		}
		
		var timeout = 1100;
		function getMarkers() {
			setTimeout('getResponseReceived(); checkForValidResponse();', timeout);
		}
		
		var flags = "";
		function getResponseReceived() {
			var responseReceived = $("#response").html();
			try {
				flags = JSON.parse(responseReceived);
			} catch(e) {
				checkForValidResponse();
				console.log('NOT JSON - Response not received yet!');
			}
		}
		
		function checkForValidResponse() {
			if (flags == '')
				setTimeout('getResponseReceived(); checkForValidResponse();', timeout);
			else
				 addMarkers();
		}
		
		function sortJSONByDistance() {
			var sorted = false;
			while (!sorted) {
				sorted = true;
				for (var i=0; i<flags.locations.length-1; i++) {
					var my_location = new google.maps.LatLng(getCurrentLat(), getCurrentLng());
					var location1 = new google.maps.LatLng(flags.locations[i].lat, flags.locations[i].lng);
					var location2 = new google.maps.LatLng(flags.locations[i+1].lat, flags.locations[i+1].lng);
					if (calcDistance(my_location, location1) > calcDistance(my_location, location2)) {
						//console.log(calcDistance(my_location, location1) + ' : ' + calcDistance(my_location, location2) + ' : ' + flags.locations[i].address + ' : ' + flags.locations[i+1].address);
						sorted = false;
						
						var temp_locations = flags.locations[i];
						var temp_sharings = flags.sharings[i];
						var temp_items = flags.items[i];
						var temp_users = flags.users[i];
						var temp_ratings = flags.ratings[i];
						
						flags.locations[i] = flags.locations[i+1];
						flags.locations[i+1] = temp_locations;
						
						flags.sharings[i] = flags.sharings[i+1];
						flags.sharings[i+1] = temp_sharings;
						
						flags.items[i] = flags.items[i+1];
						flags.items[i+1] = temp_items;
						
						flags.users[i] = flags.users[i+1];
						flags.users[i+1] = temp_users;
						
						flags.ratings[i] = flags.ratings[i+1];
						flags.ratings[i+1] = temp_ratings;
					}
				}
			}
		}
		
		var map, markers, infowindow, infowindowContent;
		function addMarkers() {
			
			sortJSONByDistance();
			removeItemsOutOfLocationRange();
			markers = [];
			map = new google.maps.Map(document.getElementById('map'), {
			  zoom: 10,
			  center: new google.maps.LatLng(51.5, 0.08),
			  mapTypeId: google.maps.MapTypeId.ROADMAP
			});

			infowindow = new google.maps.InfoWindow();
			
			var bounds = new google.maps.LatLngBounds();
			
			for (var i = 0; i < flags.locations.length; i++) {  
			  positionObject = new google.maps.LatLng(flags.locations[i].lat, flags.locations[i].lng);
			  markers.push(new google.maps.Marker({
				position: positionObject,
				map: map,
				animation: google.maps.Animation.DROP
			  }));

			  google.maps.event.addListener(markers[i], 'click', (function(i) {
				return function() {
				  infowindow.setContent(getContentForMarkerAt(i));
				  infowindow.open(map, markers[i]);
				  //toggleMenuOrNot();
				}
			  })(i));
			  
			  bounds.extend(positionObject);
			}
			
			map.fitBounds(bounds);
			
			displayListForMarkers();
			/* // For Testing
			time3 = new Date();
			console.log('Time between receiving the response and adding the markers:');
			console.log(time3 - time2);
			console.log('Total time between sending the request and adding the received markers:');
			console.log(time3 - time1);
			// End of Testing */
		}
		
		function displayListForMarkers() {
			htmlForListForMarkers = '<div class="panel-heading">Results found: <span class="badge">' + flags.locations.length + '</span></div>';
			for (i=0; i<flags.locations.length; i++) {
				htmlForListForMarkers += '<button type="button" class="list-group-item" onMouseOver="bounceMarker(' + i + ');" onclick="openInfoWindow(' + i + '); toggleMenuOrNot();">' + flags.items[i].name;
				var distance = calcDistance(new google.maps.LatLng(getCurrentLat(), getCurrentLng()), new google.maps.LatLng(flags.locations[i].lat, flags.locations[i].lng));
				var distanceLabel = '<div class="text-right"> <span class="label label-default">' + distance + ' km</span></div>';
				htmlForListForMarkers += distanceLabel + '</button>';
				
			}
			document.getElementById('list-for-markers').innerHTML = htmlForListForMarkers;
		}
		
		function hideListForMarkers() {
			document.getElementById('list-for-markers').innerHTML = '';
		}
		
		function bounceMarker(index) {
			markers[index].setAnimation(google.maps.Animation.BOUNCE);
			setTimeout(function(){
				markers[index].setAnimation(null);
			}, 1400);
		}
		
		function openInfoWindow(index) {
			infowindow.setContent(getContentForMarkerAt(index));
			infowindow.open(map, markers[index]);
		}
		
		function getContentForMarkerAt(index) {
			
			var content = '<div class="" style="max-height: 200px; overflow-y: scroll; width: 200px; word-wrap: break-word;">';
			content += '<h5>' + flags.items[index].category + ': ';
			content += flags.items[index].name + '</h5><br>';
			content += flags.items[index].description + '<hr>';
			content += '<h5>By: ' + flags.users[index].firstname + ' ' + flags.users[index].surname + '</h5><br>';
			// If the current user had already booked the item previously, then display the sharer's phone number and email address
			if (flags.sharings[index].giver_id != getCurrentUserId() && flags.sharings[index].taker_id == getCurrentUserId()) {
				content += 'Phone: ' + flags.users[index].phone + '<br>';
				content += 'Email: <a href="mailto:' + flags.users[index].email + '">' + flags.users[index].email + '</a><br><br>';
			}
			content += getRatingContent(index) + '<hr>';
			content += getActionButton(index);
			content += '</div>';
			return content;
		}
		
		function getRatingContent(index) {
			var ratingContent = '';
			// If the user has booked AND received the item but did give a rating, then display the rating option (i.e. the stars icons)
			if (flags.sharings[index].rated == 'NO' && flags.sharings[index].giver_id != getCurrentUserId() && flags.sharings[index].taker_id == getCurrentUserId() && flags.sharings[index].trade_status == 'COMPLETE') {
				ratingContent = '<div class="rating-content">Rate this user: <br>';
				for (var i=1, j=i+8; i<=5; i++, j++)
					ratingContent += '<span class="glyphicon glyphicon-star-empty" aria-hidden="true" style="font-size: ' + j + 'pt; cursor: pointer;" onMouseOver="star(this, ' + i + ')" onMouseOut="star(this, null)" onClick="setStarNumber(' + i + '); setSharingIdToAdd(' + flags.sharings[index].sharing_id + '); setUserToRate(' + flags.sharings[index].giver_id + '); ajaxFunction(\'rate_item\');"></span>';
				ratingContent += '</div>';
			}
			// Display the ratings for each user
			ratingContent += '<b>' + Number(flags.ratings[index].rating_average).toFixed(1) + '</b> average ratings by <b>' + Number(flags.ratings[index].rating_count) + '</b> users<br>';
			
			return ratingContent;
		}
		
		function star(element, starValue) {
			if (starValue == null)
				element.className="glyphicon glyphicon-star-empty";
			else
				element.className="glyphicon glyphicon-star";
		}
		
		function getActionButton(index) {
			var actionButton;
			
			// If the item is available for current user to book, then add a 'Book Item' button
			if (flags.sharings[index].giver_id != getCurrentUserId() && flags.sharings[index].taker_id == 0)
				actionButton = '<button type="button" class="btn btn-success" onclick="setSharingIdToAdd(' + flags.sharings[index].sharing_id + '); ajaxFunction(\'book_item\');"><span class="glyphicon glyphicon-check" aria-hidden="true"></span> Book It</button>';
			// If the current user had already booked the item previously, then add the following action buttons
			else if (flags.sharings[index].giver_id != getCurrentUserId() && flags.sharings[index].taker_id == getCurrentUserId()) {
				// If the item has not been received yet then add the buttons: 'Received Item' and 'Cancel Booking'
				if (flags.sharings[index].trade_status == 'INCOMPLETE') {
					actionButton = '<button type="button" class="btn btn-primary" onclick="setSharingIdToAdd(' + flags.sharings[index].sharing_id + '); ajaxFunction(\'received_item\');"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Item Received</button><br><br>';
					actionButton += '<button type="button" class="btn btn-warning" onclick="setSharingIdToAdd(' + flags.sharings[index].sharing_id + '); ajaxFunction(\'cancel_booked_item\');"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Cancel Booking</button>';
				}
				// If the item has been received too, then add an inactive button stating that the item has been 'Booked' and 'Received'
				else
					actionButton = '<button type="button" class="btn btn-primary disabled"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Booked & Received</button>';
			}
			// If the item is shared (i.e. offered to others) by the current user AND has not been taken yet, then add a 'Cancel Sharing' button
			else if (flags.sharings[index].giver_id == getCurrentUserId() && flags.sharings[index].taker_id == 0) {
				actionButton = '<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#editItemModal" onclick="populateFieldsForEditItemModal(' + index + ');setSharingIdToAdd(' + flags.sharings[index].sharing_id + '); setItemId(' + flags.items[index].item_id + '); "><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit Item</button><br><br>';
				actionButton += '<button type="button" class="btn btn-danger" onclick="setSharingIdToAdd(' + flags.sharings[index].sharing_id + '); setItemId(' + flags.items[index].item_id + '); ajaxFunction(\'remove_shared_item\');"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Remove Item</button>';
			}
			// If the item is not shared by current user and already booked by someone else, then add a 'No Action' button
			else if (flags.sharings[index].giver_id != getCurrentUserId() && flags.sharings[index].taker_id != 0 && flags.sharings[index].taker_id != getCurrentUserId())
				actionButton = '<button type="button" class="btn btn-primary disabled"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Already Booked</button>';
			// Just repeat the previous one
			else
				actionButton = '<button type="button" class="btn btn-primary disabled"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Already Booked</button>';
			
			return actionButton;
		}
		
		function populateFieldsForEditItemModal(index) {
			document.getElementById('edit-item-title').value = flags.items[index].name;
			document.getElementById('edit-item-category').value = flags.items[index].category;
			document.getElementById('edit-item-description').innerHTML = flags.items[index].description;
			//document.getElementById('edit-item-trade-type').value = flags.sharings[index].trade_type;
			document.getElementById('edit-item-location').value = flags.locations[index].location_id;
		}
		
		function populateFieldsForEditPersonalInfoModal() {
			document.getElementById('firstname-text').value = currentUser.users[0].firstname;
			document.getElementById('surname-text').value = currentUser.users[0].surname;
			document.getElementById('email-text').value = currentUser.users[0].email;
			document.getElementById('phone-text').value = currentUser.users[0].phone;
		}
		
		function calcDistance(location1, location2) {
			var difference = (google.maps.geometry.spherical.computeDistanceBetween(location1, location2) / 1000).toFixed(2);
			return Number(difference);
		}
		
		function getCurrentUserId() {
			var userId = '<?php echo $_SESSION['session'] ?>';
			return userId;
		}
		
		function removeItemsOutOfLocationRange() {
			var fieldItemLocationRange = document.getElementById('search-item-location-range').value;
			if (fieldItemLocationRange != 'All') {
				for (i=0; i<flags.locations.length; i++) {
					var distance = calcDistance(new google.maps.LatLng(getCurrentLat(), getCurrentLng()), new google.maps.LatLng(flags.locations[i].lat, flags.locations[i].lng));
					if (distance > fieldItemLocationRange) {
						flags.locations.splice(i, 1);
						flags.items.splice(i, 1);
						flags.sharings.splice(i, 1);
						flags.users.splice(i, 1);
						flags.ratings.splice(i, 1);
						i--;
					}
				}
			}
		}
		
		var currentUser = "";
		var currentUserLocations = "";
		var currentUserSharedItems = "";
		
		function getCurrentUserJSON() {
			var responseReceived = $("#current-user-info").html();
			try {
				currentUser = JSON.parse(responseReceived);
				var userInfoDiv = document.getElementById('userinfo')
				userInfoDiv.innerHTML = 'Welcome: ';
				userInfoDiv.innerHTML += currentUser.users[0].firstname + ' ';
				userInfoDiv.innerHTML += currentUser.users[0].surname;
			}
			catch(e) {console.log('NOT JSON - Response not received yet!');}
		}
		
		function getCurrentUserLocationsJSON() {
			var responseReceived = $("#current-user-stored-locations").html();
			try {
				currentUserLocations = JSON.parse(responseReceived);
			} catch(e) {console.log('NOT JSON - Response not received yet!');}
		}
		
		function getCurrentUserSharedItemsJSON() {
			var responseReceived = $("#current-user-shared-items").html();
			try {currentUserSharedItems = JSON.parse(responseReceived);}
			catch(e) {console.log('NOT JSON - Response not received yet!');}
		}
		
		function updateSharedItemLocationsList() {
			
			contentForSharedItem = '<label for="share-item-location">Select Address</label>';
			contentForSharedItem += '<select class="form-control" id="share-item-location">';
			contentForRemovingItem = '<label for="current-user-locations">My Stored Locations</label>';
			contentForRemovingItem += '<select class="form-control" id="current-user-locations">';
			contentForEditingItem = '<label for="edit-item-location">My Stored Locations</label>';
			contentForEditingItem += '<select class="form-control" id="edit-item-location">';
			for (i=0; i<currentUserLocations.locations.length; i++) {
				contentForSharedItem += '<option value="' + currentUserLocations.locations[i].location_id + '">';
				contentForSharedItem += currentUserLocations.locations[i].address + '</option>';
				contentForRemovingItem += '<option value="' + currentUserLocations.locations[i].location_id + '">';
				contentForRemovingItem += currentUserLocations.locations[i].address + '</option>';
				contentForEditingItem += '<option value="' + currentUserLocations.locations[i].location_id + '">';
				contentForEditingItem += currentUserLocations.locations[i].address + '</option>';
			}
			contentForSharedItem += '</select>';
			contentForRemovingItem += '</select>';
			contentForEditingItem += '</select>';
			document.getElementById('share-item-location-container').innerHTML = contentForSharedItem;
			document.getElementById('current-user-locations-container').innerHTML = contentForRemovingItem;
			document.getElementById('edit-item-location-container').innerHTML = contentForEditingItem;
		}
		
		function sampleInitMap() {
		  var myLatlng = {lat: -25.363, lng: 131.044};

		  var map = new google.maps.Map(document.getElementById('map'), {
			zoom: 4,
			center: myLatlng
		  });

		  var marker = new google.maps.Marker({
			position: myLatlng,
			map: map,
			title: 'Click to zoom'
		  });

		  marker.addListener('click', function() {
			map.setZoom(8);
			map.setCenter(marker.getPosition());
		  });
		}
    </script>
    <script async defer
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCVmL9ZOJlcFK7Vu0Tz7OzzTQIhQ87-uDk&callback=initMap&libraries=geometry">
    </script>
	
	</div>
	
  </body>
</html>