<!--
//Browser Support Code

// Static Variables (Strings)
var ADD_SHARED_ITEMS = 'add_shared_items';
var GET_SHARED_ITEMS = 'get_shared_items';
var GET_CURRENT_USER = 'get_current_user';
var GET_CURRENT_USER_LOCATIONS = 'get_current_user_locations';
var GET_CURRENT_USER_SHARED_ITEMS = 'get_current_user_shared_items';
var ADD_LOCATION = 'add_location';
var REMOVE_LOCATION = 'remove_location';
var BOOK_ITEM = 'book_item';
var CANCEL_BOOKED_ITEM = 'cancel_booked_item';
var REMOVE_SHARED_ITEM = 'remove_shared_item';
var EDIT_ITEM = 'edit_item';
var RECEIVED_ITEM = 'received_item';
var RATE_ITEM = 'rate_item';
var EDIT_PERSONAL_INFO = 'edit_personal_info';
// var time1, time2, time3; // <-- For Testing

function ajaxFunction(queryType){
   var ajaxRequest;  // The variable that makes Ajax possible!
   
   try {
	  // Opera 8.0+, Firefox, Safari
	  ajaxRequest = new XMLHttpRequest();
   }catch (e) {
	  // Internet Explorer Browsers
	  try {
		 ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
	  }catch (e) {
		 try{
			ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
		 }catch (e){
			// Error occurred
			alert("Your browser crashed !");
			return false;
		 }
	  }
   }
   
   // Creating a function that will retrieve data from server and then update the DIV in the same webpage.
		
   ajaxRequest.onreadystatechange = function(){
	  if(ajaxRequest.readyState == 4){
		 toDo(ajaxRequest, queryType);
	  }
   }
   
   // Now get the value from user and pass it to
   // server script.
	
	var queryString = "";
	
	if (queryType == GET_SHARED_ITEMS)
		queryString = getSharedItems();
	
	else if (queryType == ADD_SHARED_ITEMS)
		queryString = addSharedItemToDB();
	
	else if (queryType == GET_CURRENT_USER)
		queryString = getCurrentUserInfoQuery();
	
	else if (queryType == GET_CURRENT_USER_LOCATIONS)
		queryString = getCurrentUserLocationsQuery();
	
	else if (queryType == GET_CURRENT_USER_SHARED_ITEMS)
		queryString = getCurrentUserSharedItemsQuery();
	
	else if (queryType == ADD_LOCATION)
		queryString = addLocationQuery();
	
	else if (queryType == REMOVE_LOCATION)
		queryString = removeLocationQuery();
	
	else if (queryType == BOOK_ITEM)
		queryString = bookItem();
	
	else if (queryType == CANCEL_BOOKED_ITEM)
		queryString = cancelBookedItem();
	
	else if (queryType == REMOVE_SHARED_ITEM)
		queryString = removeSharedItem();
	
	else if (queryType == EDIT_ITEM)
		queryString = editSharedItem();
	
	else if (queryType == RECEIVED_ITEM)
		queryString = receivedSharedItem();
	
	else if (queryType == RATE_ITEM)
		queryString = rateItem();
	
	else if (queryType == EDIT_PERSONAL_INFO)
		queryString = editPersonalInfo();
	
	else
		queryString = getSharedItems();
   ajaxRequest.open("GET", "ajax-php.php" + queryString, true);
   ajaxRequest.send(null); 
   // time1 = new Date(); // <-- For Testing
}

function getSharedItems() {
	// Build the URL parameters
	var fieldItemKeyword = document.getElementById('search-item-keyword').value;
	var fieldItemCategory = document.getElementById('search-item-category').value;
	//var fieldItemTradeType = document.getElementById('search-item-trade-type').value;
	var queryStringBuilt = "?fieldItemKeyword=" + fieldItemKeyword;
	queryStringBuilt += "&fieldItemCategory=" + fieldItemCategory;
	//queryStringBuilt += "&fieldItemTradeType=" + fieldItemTradeType;
	queryStringBuilt += "&fieldGiverId=" + getCurrentUserId();
	queryStringBuilt += "&fieldOnlyShowItemsSharedOrBooked=" + showItemsSharedOrBooked;
	queryStringBuilt += "&queryType=" + GET_SHARED_ITEMS;
	
	// Show a progress bar until the map contents are rendered
	whileLoading(GET_SHARED_ITEMS, 'lock');
	
	return queryStringBuilt;
}

function addSharedItemToDB() {
	// Get the values from the 'Share Item' modal and build the URL parameters
	var shareQuery = "";
	var sharedItemTitle = document.getElementById('shared-item-title').value;
	var sharedItemCategory = document.getElementById('share-item-category').value;
	var sharedItemDescription = document.getElementById('shared-item-description').value;
	//var sharedItemTradeType = document.getElementById('share-item-trade-type').value;
	var sharedItemLocation = document.getElementById('share-item-location').value;
	//if(sharedItemTitle != "" || sharedItemCategory != "" || sharedItemDescription != "" || sharedItemTradeType != "" || sharedItemTradeType != "" || sharedItemTradeType != "")
	//	shareQuery = "?sharedItemTitle=" + sharedItemTitle + "&sharedItemCategory=" + sharedItemCategory + "&sharedItemDescription=" + sharedItemDescription + "&sharedItemTradeType=" + sharedItemTradeType + "&sharedItemGiverId=" + getCurrentUserId() + "&sharedItemLocationId=" + sharedItemLocation + "&queryType=" + ADD_SHARED_ITEMS;
	if(sharedItemTitle != "" || sharedItemCategory != "" || sharedItemDescription != "")
		shareQuery = "?sharedItemTitle=" + sharedItemTitle + "&sharedItemCategory=" + sharedItemCategory + "&sharedItemDescription=" + sharedItemDescription + "&sharedItemGiverId=" + getCurrentUserId() + "&sharedItemLocationId=" + sharedItemLocation + "&queryType=" + ADD_SHARED_ITEMS;
	
	// Disable input fields and show a progress bar
	whileLoading(ADD_SHARED_ITEMS, 'lock');
	
	return shareQuery;
}

function getCurrentUserInfoQuery() {
	// Get the values from the 'Share Item' modal and build the URL parameters
	var shareQuery = "?fieldUserId=" + getCurrentUserId();
	shareQuery += "&queryType=" + GET_CURRENT_USER;
	
	// Disable input fields and show a progress bar
	whileLoading(GET_CURRENT_USER, 'lock');
	
	return shareQuery;
}

function getCurrentUserLocationsQuery() {
	// Get the values from the 'Share Item' modal and build the URL parameters
	var shareQuery = "?fieldUserId=" + getCurrentUserId();
	shareQuery += "&queryType=" + GET_CURRENT_USER_LOCATIONS;
	
	// Disable input fields and show a progress bar
	whileLoading(GET_CURRENT_USER_LOCATIONS, 'lock');
	
	return shareQuery;
}

function getCurrentUserSharedItemsQuery() {
	// Get the values from the 'Share Item' modal and build the URL parameters
	var shareQuery = "?fieldUserId=" + getCurrentUserId();
	shareQuery += "&queryType=" + GET_CURRENT_USER_SHARED_ITEMS;
	
	// Disable input fields and show a progress bar
	whileLoading(GET_CURRENT_USER_SHARED_ITEMS, 'lock');
	
	return shareQuery;
}

function addLocationQuery() {
	// Get the values from the 'Share Item' modal and build the URL parameters
	var shareQuery = "?fieldUserId=" + getCurrentUserId();
	shareQuery += "&fieldAddress=" + document.getElementById('save-current-user-location-address').value;
	shareQuery += "&fieldLocationLat=" + getCurrentLat();
	shareQuery += "&fieldLocationLng=" + getCurrentLng();
	shareQuery += "&queryType=" + ADD_LOCATION;
	
	// Disable input fields and show a progress bar
	whileLoading(ADD_LOCATION, 'lock');
	
	return shareQuery;
}

function removeLocationQuery() {
	// Get the values from the 'Share Item' modal and build the URL parameters
	var locationsList = document.getElementById('current-user-locations');
	var shareQuery = "?fieldUserId=" + getCurrentUserId();
	shareQuery += "&fieldLocationId=" + locationsList[locationsList.selectedIndex].getAttribute('value');
	shareQuery += "&queryType=" + REMOVE_LOCATION;
	
	// Disable input fields and show a progress bar
	whileLoading(REMOVE_LOCATION, 'lock');
	
	return shareQuery;
}

function bookItem() {
	// Get the values from the 'Share Item' modal and build the URL parameters
	var shareQuery = "?fieldUserId=" + getCurrentUserId();
	shareQuery += "&fieldSharingId=" + getSharingIdToAdd();
	shareQuery += "&queryType=" + BOOK_ITEM;
	
	// Disable input fields and show a progress bar
	whileLoading(BOOK_ITEM, 'lock');
	
	return shareQuery;
}

function cancelBookedItem() {
	// Get the values from the 'Share Item' modal and build the URL parameters
	var shareQuery = "?fieldSharingId=" + getSharingIdToAdd();
	shareQuery += "&queryType=" + CANCEL_BOOKED_ITEM;
	
	// Disable input fields and show a progress bar
	whileLoading(CANCEL_BOOKED_ITEM, 'lock');
	
	return shareQuery;
}

function removeSharedItem() {
	// Get the values from the 'Share Item' modal and build the URL parameters
	var shareQuery = "?fieldSharingId=" + getSharingIdToAdd();
	shareQuery += "&fieldItemId=" + getItemId();
	shareQuery += "&queryType=" + REMOVE_SHARED_ITEM;
	
	// Disable input fields and show a progress bar
	whileLoading(REMOVE_SHARED_ITEM, 'lock');
	
	return shareQuery;
}

function editSharedItem() {
	// Get the values from the 'Share Item' modal and build the URL parameters
	var shareQuery = "?editItemTitle=" + document.getElementById('edit-item-title').value;
	shareQuery += "&editItemCategory=" + document.getElementById('edit-item-category').value;
	shareQuery += "&editItemDescription=" + document.getElementById('edit-item-description').value;
	//shareQuery += "&editItemTradeType=" + document.getElementById('edit-item-trade-type').value;
	shareQuery += "&editItemLocationId=" + document.getElementById('edit-item-location').value;
	shareQuery += "&fieldSharingId=" + getSharingIdToAdd();
	shareQuery += "&fieldItemId=" + getItemId();
	shareQuery += "&queryType=" + EDIT_ITEM;
	
	// Disable input fields and show a progress bar
	whileLoading(EDIT_ITEM, 'lock');
	
	return shareQuery;
}

function receivedSharedItem() {
	// Get the values from the 'Share Item' modal and build the URL parameters
	shareQuery = "?fieldSharingId=" + getSharingIdToAdd();
	shareQuery += "&queryType=" + RECEIVED_ITEM;
	
	// Disable input fields and show a progress bar
	whileLoading(RECEIVED_ITEM, 'lock');
	
	return shareQuery;
}

function rateItem() {
	// Get the values from the 'Share Item' modal and build the URL parameters
	shareQuery = "?fieldRatingStar=" + getStarNumber();
	shareQuery += "&fieldUserToRate=" + getUserToRate();
	shareQuery += "&fieldSharingId=" + getSharingIdToAdd();
	shareQuery += "&queryType=" + RATE_ITEM;
	
	// Disable input fields and show a progress bar
	whileLoading(RATE_ITEM, 'lock');
	
	return shareQuery;
}

function editPersonalInfo() {
	// Get the values from the 'Share Item' modal and build the URL parameters
	shareQuery = "?fieldFirstname=" + document.getElementById('firstname-text').value;
	shareQuery += "&fieldSurname=" + document.getElementById('surname-text').value;
	shareQuery += "&fieldEmail=" + document.getElementById('email-text').value;
	shareQuery += "&fieldPhone=" + document.getElementById('phone-text').value;
	shareQuery += "&fieldUserId=" + getCurrentUserId();
	shareQuery += "&queryType=" + EDIT_PERSONAL_INFO;
	
	// Disable input fields and show a progress bar
	whileLoading(EDIT_PERSONAL_INFO, 'lock');
	
	return shareQuery;
}

function toDo(ajaxRequest, queryType) {
	// Specifying what to do with the AJAX Response once it is ready
	var ajaxDisplay = document.getElementById('response');
	
	if (queryType == GET_SHARED_ITEMS) {
		ajaxDisplay.innerHTML = JSON.parse(ajaxRequest.responseText);
		/* // For Testing
		time2 = new Date();
		console.log('Time between making the request and receiving the response:');
		console.log(time2 - time1);
		// End of Testing */
		getMarkers();
	}
	else if (queryType == ADD_SHARED_ITEMS) {
		ajaxDisplay.innerHTML = ajaxRequest.responseText;
		whileLoading(ADD_SHARED_ITEMS, 'unlock');
	}
	else if (queryType == GET_CURRENT_USER) {
		document.getElementById('current-user-info').innerHTML = JSON.parse(ajaxRequest.responseText);
		whileLoading(GET_CURRENT_USER, 'unlock');
		getCurrentUserJSON();
	}
	else if (queryType == GET_CURRENT_USER_LOCATIONS) {
		document.getElementById('current-user-stored-locations').innerHTML = JSON.parse(ajaxRequest.responseText);
		whileLoading(GET_CURRENT_USER_LOCATIONS, 'unlock');
		getCurrentUserLocationsJSON();
		updateSharedItemLocationsList();
	}
	else if (queryType == GET_CURRENT_USER_SHARED_ITEMS) {
		document.getElementById('current-user-shared-items').innerHTML = JSON.parse(ajaxRequest.responseText);
		whileLoading(GET_CURRENT_USER_SHARED_ITEMS, 'unlock');
		getCurrentUserSharedItemsJSON();
	}
	else if (queryType == ADD_LOCATION) {
		document.getElementById('current-user-location-add').innerHTML = JSON.parse(ajaxRequest.responseText);
		whileLoading(ADD_LOCATION, 'unlock');
		ajaxFunction("get_current_user_locations");
	}
	else if (queryType == REMOVE_LOCATION) {
		var queryResponse = document.getElementById('current-user-locations-remove');
		if (JSON.parse(ajaxRequest.responseText) == false){
			queryResponse.innerHTML = '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><b>Location could not be removed!</b><br>You may have an incomplete share of an item at this location.</div>';
			setTimeout(function () {$(".alert").alert('close');}, 5000);
			setTimeout(function () {$('#currentUserLocationsModal').modal('hide');}, 6000);
		} else {
			queryResponse.innerHTML = '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><b>Done</b><br>Location has been removed from your locations list.</div>';
			setTimeout(function () {$(".alert").alert('close');}, 2000);
			setTimeout(function () {$('#currentUserLocationsModal').modal('hide');}, 3000);
		}
		whileLoading(REMOVE_LOCATION, 'unlock');
		ajaxFunction("get_current_user_locations");
	}
	else if (queryType == BOOK_ITEM) {
		document.getElementById('generic-response').innerHTML = JSON.parse(ajaxRequest.responseText);
		whileLoading(BOOK_ITEM, 'unlock');
		ajaxFunction("GET_SHARED_ITEMS");
	}
	else if (queryType == CANCEL_BOOKED_ITEM) {
		document.getElementById('generic-response').innerHTML = JSON.parse(ajaxRequest.responseText);
		whileLoading(CANCEL_BOOKED_ITEM, 'unlock');
		ajaxFunction("GET_SHARED_ITEMS");
	}
	else if (queryType == REMOVE_SHARED_ITEM) {
		document.getElementById('generic-response').innerHTML = JSON.parse(ajaxRequest.responseText);
		whileLoading(REMOVE_SHARED_ITEM, 'unlock');
		ajaxFunction("GET_SHARED_ITEMS");
	}
	else if (queryType == EDIT_ITEM) {
		document.getElementById('generic-response').innerHTML = JSON.parse(ajaxRequest.responseText);
		whileLoading(EDIT_ITEM, 'unlock');
		ajaxFunction("GET_SHARED_ITEMS");
	}
	else if (queryType == RECEIVED_ITEM) {
		document.getElementById('generic-response').innerHTML = JSON.parse(ajaxRequest.responseText);
		whileLoading(RECEIVED_ITEM, 'unlock');
		ajaxFunction("GET_SHARED_ITEMS");
	}
	else if (queryType == RATE_ITEM) {
		document.getElementById('generic-response').innerHTML = JSON.parse(ajaxRequest.responseText);
		whileLoading(RATE_ITEM, 'unlock');
		ajaxFunction("GET_SHARED_ITEMS");
	}
	else if (queryType == EDIT_PERSONAL_INFO) {
		document.getElementById('generic-response').innerHTML = JSON.parse(ajaxRequest.responseText);
		whileLoading(EDIT_PERSONAL_INFO, 'unlock');
	}
	else {
		ajaxDisplay.innerHTML = JSON.parse(ajaxRequest.responseText);
		getMarkers();
	}
}

function whileLoading(queryType, lockFlag) {
	
	if (queryType == GET_SHARED_ITEMS) {
		// Display a progress bar until the map contents are rendered
		if (lockFlag == 'lock') {
			document.getElementById('map').innerHTML = '<div class="progress" style="margin: 25px 10px;"><div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"><span class="sr-only">In Progress</span></div></div>';
		}
		else {
			
		}
	}
	else if (queryType == ADD_SHARED_ITEMS) {
		if (lockFlag == 'lock') {
			// Disable input fields and show a progress bar
			document.getElementById('shared-item-title').disabled = true;
			document.getElementById('share-item-category').disabled = true;
			document.getElementById('shared-item-description').disabled = true;
			//document.getElementById('share-item-trade-type').disabled = true;
			document.getElementById('share-item-location').disabled = true;
			document.getElementById('shared-item-submit').disabled = true;
			document.getElementById('shared-item-clear').disabled = true;
			document.getElementById('shared-item-progress-bar').style.visibility = "visible";
			document.getElementById('shared-item-progress-bar').style.display = "block";
		}
		else {
			// Enable input fields and show a progress bar
			document.getElementById('shared-item-title').disabled = false;
			document.getElementById('share-item-category').disabled = false;
			document.getElementById('shared-item-description').disabled = false;
			//document.getElementById('share-item-trade-type').disabled = false;
			document.getElementById('share-item-location').disabled = false;
			document.getElementById('shared-item-submit').disabled = false;
			document.getElementById('shared-item-clear').disabled = false;
			document.getElementById('shared-item-progress-bar').style.visibility = "hidden";
			document.getElementById('shared-item-progress-bar').style.display = "none";
			$('#shareItemModal').modal('hide');
			toggleMenu();
		}
	}
	else if (queryType == GET_CURRENT_USER) {
		if (lockFlag == 'lock') {/* Disable input fields and show a progress bar*/}
		else {/* Enable input fields and show a progress bar*/}
	}
	else if (queryType == GET_CURRENT_USER_LOCATIONS) {
		if (lockFlag == 'lock') {/* Disable input fields and show a progress bar*/}
		else {/* Enable input fields and show a progress bar*/}
	}
	else if (queryType == GET_CURRENT_USER_SHARED_ITEMS) {
		if (lockFlag == 'lock') {/* Disable input fields and show a progress bar*/}
		else {/* Enable input fields and show a progress bar*/}
	}
	else if (queryType == ADD_LOCATION) {
		if (lockFlag == 'lock') {/* Disable input fields and show a progress bar*/
			document.getElementById('save-current-user-location-address').disabled = true;
			document.getElementById('save-current-user-location-submit').disabled = true;
			document.getElementById('save-current-user-location-progress-bar').style.visibility = "visible";
			document.getElementById('save-current-user-location-progress-bar').style.display = "block";
		}
		else {/* Enable input fields and show a progress bar*/
			document.getElementById('save-current-user-location-address').disabled = false;
			document.getElementById('save-current-user-location-submit').disabled = false;
			document.getElementById('save-current-user-location-progress-bar').style.visibility = "hidden";
			document.getElementById('save-current-user-location-progress-bar').style.display = "none";
			$('#saveCurrentUserLocationModal').modal('hide');
			ajaxFunction("get_current_user_locations");
		}
	}
	else if (queryType == REMOVE_LOCATION) {
		if (lockFlag == 'lock') {/* Disable input fields and show a progress bar*/
			document.getElementById('current-user-locations').disabled = true;
			document.getElementById('current-user-locations-remove').disabled = true;
			document.getElementById('current-user-locations-progress-bar').style.visibility = "visible";
			document.getElementById('current-user-locations-progress-bar').style.display = "block";
		}
		else {/* Enable input fields and show a progress bar*/
			document.getElementById('current-user-locations').disabled = false;
			document.getElementById('current-user-locations-remove').disabled = false;
			document.getElementById('current-user-locations-progress-bar').style.visibility = "hidden";
			document.getElementById('current-user-locations-progress-bar').style.display = "none";
		}
	}
	else if (queryType == BOOK_ITEM) {
		if (lockFlag == 'lock') {/* Disable input fields and show a progress bar*/}
		else {/* Enable input fields and show a progress bar*/}
	}
	else if (queryType == CANCEL_BOOKED_ITEM) {
		if (lockFlag == 'lock') {/* Disable input fields and show a progress bar*/}
		else {/* Enable input fields and show a progress bar*/}
	}
	else if (queryType == REMOVE_SHARED_ITEM) {
		if (lockFlag == 'lock') {/* Disable input fields and show a progress bar*/}
		else {/* Enable input fields and show a progress bar*/}
	}
	else if (queryType == EDIT_ITEM) {
		if (lockFlag == 'lock') {/* Disable input fields and show a progress bar*/}
		else {/* Enable input fields and show a progress bar*/}
	}
	else if (queryType == RECEIVED_ITEM) {
		if (lockFlag == 'lock') {/* Disable input fields and show a progress bar*/}
		else {/* Enable input fields and show a progress bar*/}
	}
	else if (queryType == RATE_ITEM) {
		if (lockFlag == 'lock') {/* Disable input fields and show a progress bar*/}
		else {/* Enable input fields and show a progress bar*/}
	}
	else if (queryType == EDIT_PERSONAL_INFO) {
		if (lockFlag == 'lock') {/* Disable input fields and show a progress bar*/
			document.getElementById('firstname-text').disabled = true;
			document.getElementById('surname-text').disabled = true;
			document.getElementById('email-text').disabled = true;
			document.getElementById('edit-personal-info-submit').disabled = true;
			document.getElementById('edit-personal-info-clear').disabled = true;
			document.getElementById('phone-text').disabled = true;
			document.getElementById('edit-personal-info-progress-bar').style.visibility = "visible";
			document.getElementById('edit-personal-info-progress-bar').style.display = "block";
		}
		else {/* Enable input fields and show a progress bar*/
			document.getElementById('firstname-text').disabled = false;
			document.getElementById('surname-text').disabled = false;
			document.getElementById('email-text').disabled = false;
			document.getElementById('phone-text').disabled = false;
			document.getElementById('edit-personal-info-submit').disabled = false;
			document.getElementById('edit-personal-info-clear').disabled = false;
			document.getElementById('edit-personal-info-progress-bar').style.visibility = "hidden";
			document.getElementById('edit-personal-info-progress-bar').style.display = "none";
			$('#editPersonalInfoModal').modal('hide');
			ajaxFunction('get_current_user');
		}
	}
}

function getCurrentLat() {
	storeCurrentLatAndLng();
	return document.getElementById('current-latitude').innerHTML;
}

function getCurrentLng() {
	storeCurrentLatAndLng();
	return document.getElementById('current-longitude').innerHTML;
}

// Other global variables for getters and setters
var showItemsSharedOrBooked;
var sharingIdToAdd;
var itemId;
var starNumber;
var userToRate;

// Getters and Setters functions
function getShowItemsSharedOrBooked() {
	return showItemsSharedOrBooked;
}

function setShowItemsSharedOrBooked(value) {
	showItemsSharedOrBooked = value;
}

function getSharingIdToAdd() {
	return sharingIdToAdd;
}

function setSharingIdToAdd(value) {
	sharingIdToAdd = value;
}

function getItemId() {
	return itemId;
}

function setItemId(value) {
	itemId = value;
}

function getStarNumber() {
	return starNumber;
}

function setStarNumber(value) {
	starNumber = value;
}

function getUserToRate() {
	return userToRate;
}

function setUserToRate(value) {
	userToRate = value;
}
//-->