-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 12, 2016 at 09:23 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `geoshare`
--

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE IF NOT EXISTS `items` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `category` varchar(50) NOT NULL,
  `description` varchar(1000) NOT NULL,
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=51 ;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`item_id`, `name`, `category`, `description`) VALUES
(16, 'The Secret', 'Books', 'A book that talks about positive things.'),
(17, 'Green T-Shirt - Medium', 'Clothing', 'A green T-Shirt, medium sized made of 100% Egyptian cotton.'),
(23, 'Kids Chair', 'Furniture', 'A chair for kids ages 7 to 14.'),
(24, 'Table', 'Furniture', 'A 3 meters x 2 meters table for 8 individuals.'),
(27, 'Trousers', 'Clothing', 'Large Blue Trousers.'),
(33, 'Baby Pushchair', 'Baby and Nursery', 'A black reversible pushchair for a single baby with a bagging area underneath it.'),
(34, 'TV', 'Home and Garden', 'A 42 inch wide television with a black cover and a remote control.'),
(35, 'Laptop', 'Electronics and PCs', 'A small black laptop with Window XP installed on it.It functions well but has a small RAM and a low storage capacity.'),
(36, 'Bicycle', 'Sports and Leisure', 'A blue mountain bike with 6 gears.Used for 3 months.'),
(37, 'Stop Watch', 'Baby ', 'A handy green stop watch with night mode light.'),
(38, 'Trespass Jacket', 'Clothing', 'Trespass Men''s Grey/Black Jacket - Large.'),
(39, 'Scooter', 'Baby and Nursery', 'A black and blue stunt scooter.'),
(40, 'The Greatest 100', 'Books', 'A book that talks about the greatest 100 individuals who existed throughout history.'),
(41, 'Ford Car', 'Motors', 'A second-hand Ford car.(Has been used for 5 years)'),
(42, 'Beards Trimmer', 'Health and Beauty', 'Beard and stubble trimmers.Hair-Related.[Second-Hand]'),
(43, 'Black Women''s Shoe', 'Baby and Nursery', 'Black Women''s Shoe - Size 39'),
(44, 'Sofa', 'Furniture', 'Red Sofa for 3 individuals.'),
(45, 'Digital Camera', 'Electronics and PCs', 'A digital camera with high resolution and high memory-card capacity.'),
(46, 'Dog', 'Pets', 'A little dog aged 4 years.'),
(47, 'Trainers', 'Clothing', 'Trainers (Men''s Shoe) - Size 42.'),
(48, 'Hair Cut Machine', 'Health and Beauty', 'Hair Cut Machine with Combs of different sizes.'),
(49, 'Sunglasses', 'Jewellery and Watches', 'Silver/Black Sunglasses.Half-frame.'),
(50, 'Men''s Suit and Shirt', 'Clothing', 'A blue suit for men with a white shirt.');

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE IF NOT EXISTS `locations` (
  `location_id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(200) NOT NULL,
  `lat` float(10,6) NOT NULL,
  `lng` float(10,6) NOT NULL,
  `user_id_frkey` int(11) NOT NULL,
  `is_used` varchar(3) NOT NULL DEFAULT 'YES',
  PRIMARY KEY (`location_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`location_id`, `address`, `lat`, `lng`, `user_id_frkey`, `is_used`) VALUES
(1, 'Stratford', 51.543800, -0.014162, 5, 'YES'),
(2, 'Whitechapel', 51.516998, -0.081968, 5, 'YES'),
(3, 'Bayswater Road', 51.511398, -0.170460, 0, 'YES'),
(4, 'Vauxhall', 51.488899, -0.119648, 2, 'YES'),
(5, 'The Sherlock Holmes Museum', 51.523300, -0.159473, 0, 'YES'),
(6, 'Office', 33.975254, -3.691406, 5, 'YES'),
(7, 'Home', 51.528053, -0.033075, 4, 'YES'),
(11, 'Old Home', 30.026217, 31.260223, 4, 'YES'),
(12, 'My Family Home', 46.160809, 3.515625, 1, 'YES'),
(13, 'Market', 52.666389, -7.558594, 2, 'YES'),
(14, 'My House', 51.546978, -0.031856, 6, 'YES'),
(15, 'My Work Place', 52.727768, -1.142578, 6, 'YES'),
(16, 'My Parents House', 42.775242, 12.128906, 1, 'YES'),
(17, 'My Studio', 53.952450, -1.098633, 3, 'YES');

-- --------------------------------------------------------

--
-- Table structure for table `ratings`
--

CREATE TABLE IF NOT EXISTS `ratings` (
  `rating_id` int(11) NOT NULL AUTO_INCREMENT,
  `star_value` int(1) NOT NULL,
  `rated_user_id` int(11) NOT NULL,
  PRIMARY KEY (`rating_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `ratings`
--

INSERT INTO `ratings` (`rating_id`, `star_value`, `rated_user_id`) VALUES
(1, 3, 4),
(2, 5, 2),
(9, 3, 5),
(10, 5, 5);

-- --------------------------------------------------------

--
-- Table structure for table `sharings`
--

CREATE TABLE IF NOT EXISTS `sharings` (
  `sharing_id` int(11) NOT NULL AUTO_INCREMENT,
  `trade_status` varchar(10) NOT NULL DEFAULT 'INCOMPLETE',
  `item_id_frkey` int(11) NOT NULL,
  `location_id_frkey` int(11) NOT NULL,
  `giver_id` int(11) NOT NULL,
  `taker_id` int(11) NOT NULL DEFAULT '0',
  `rated` varchar(3) NOT NULL DEFAULT 'NO',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sharing_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=46 ;

--
-- Dumping data for table `sharings`
--

INSERT INTO `sharings` (`sharing_id`, `trade_status`, `item_id_frkey`, `location_id_frkey`, `giver_id`, `taker_id`, `rated`, `timestamp`) VALUES
(11, 'COMPLETE', 16, 1, 5, 4, 'YES', '2016-04-03 16:31:29'),
(12, 'COMPLETE', 17, 2, 5, 4, 'YES', '2016-04-03 16:35:24'),
(18, 'COMPLETE', 23, 4, 2, 5, 'YES', '2016-04-03 17:08:39'),
(19, 'INCOMPLETE', 24, 4, 2, 0, 'NO', '2016-04-03 17:17:13'),
(22, 'COMPLETE', 27, 11, 4, 5, 'YES', '2016-04-06 20:57:26'),
(28, 'INCOMPLETE', 33, 16, 1, 0, 'NO', '2016-04-10 16:17:11'),
(29, 'INCOMPLETE', 34, 12, 1, 0, 'NO', '2016-04-10 16:18:32'),
(30, 'INCOMPLETE', 35, 12, 1, 0, 'NO', '2016-04-10 16:20:12'),
(31, 'INCOMPLETE', 36, 11, 4, 0, 'NO', '2016-04-10 17:16:01'),
(32, 'INCOMPLETE', 37, 7, 4, 0, 'NO', '2016-04-10 17:20:00'),
(33, 'INCOMPLETE', 38, 13, 2, 0, 'NO', '2016-04-10 17:22:54'),
(34, 'INCOMPLETE', 39, 13, 2, 0, 'NO', '2016-04-10 17:29:38'),
(35, 'INCOMPLETE', 40, 14, 6, 0, 'NO', '2016-04-10 17:33:00'),
(36, 'INCOMPLETE', 41, 15, 6, 0, 'NO', '2016-04-10 17:35:34'),
(37, 'INCOMPLETE', 42, 14, 6, 0, 'NO', '2016-04-10 17:37:46'),
(38, 'INCOMPLETE', 43, 16, 1, 0, 'NO', '2016-04-10 18:05:34'),
(39, 'INCOMPLETE', 44, 4, 2, 0, 'NO', '2016-04-10 18:07:29'),
(40, 'INCOMPLETE', 45, 17, 3, 0, 'NO', '2016-04-10 18:11:48'),
(41, 'INCOMPLETE', 46, 17, 3, 0, 'NO', '2016-04-10 18:13:07'),
(42, 'INCOMPLETE', 47, 17, 3, 0, 'NO', '2016-04-10 18:14:16'),
(43, 'INCOMPLETE', 48, 2, 5, 0, 'NO', '2016-04-10 18:16:27'),
(44, 'INCOMPLETE', 49, 15, 6, 0, 'NO', '2016-04-10 18:20:56'),
(45, 'INCOMPLETE', 50, 6, 5, 0, 'NO', '2016-04-10 18:24:21');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `firstname` varchar(20) NOT NULL,
  `surname` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(25) NOT NULL,
  `password` varchar(30) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `firstname`, `surname`, `email`, `phone`, `password`) VALUES
(1, 'lisa.john', 'Lisa', 'John', 'l.john@hotmail.co.uk', '773213211', 'mnbvcx'),
(2, 'joe.charles', 'Joe', 'Charles', 'joe123@gmail.com', '8442231', 'lkjhgf'),
(3, 'john.trevor', 'John', 'Trevor', 'jtrevor@gmail.com', '79123', 'qwerty'),
(4, 'mohammad.nassar', 'Mohammad', 'Nassar', 'safeermohammed13190@yahoo.co.uk', '2147483647', 'asdfgh'),
(5, 'omar.abdelkafy', 'Omar', 'Abdelkafy', 'omar.abdelkafy@yahoo.co.uk', '7954321', 'poiuyt'),
(6, 'lee.parker', 'Lee', 'Parker', 'l.parker@yahoo.co.uk', '21474836471', 'zxcvbn');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
