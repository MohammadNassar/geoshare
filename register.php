<?php
	session_start();
	
	if (isset($_SESSION['session'])) {
		echo "You are logged in.";
		exit();
	}
?>

<html>

	<head>
		<style type="text/css">
		</style>
		
		<meta name="viewport" content="width=device-width, initial-scale=1">
		
		<link type="text/css" href="sidebar.css" rel="stylesheet"></link>
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
		
		<script type="text/javascript" src="ajaxwork.js"></script>
		
		<title>GEOSHARE - Register</title>
		
	</head>

	<body>
	<div class="container">
		<form role="form" action="login-verify.php" method="post">
		
		<div role="form-group">
			<br>
			
			<label for="username">Username:</label>
			<input type="text" name="username" class="form-control" placeholder="Username" /> <br />
			
			<label for="firstname">First Name:</label>
			<input type="text" name="firstname" class="form-control" placeholder="First Name" /> <br />
			
			<label for="surname">Surname:</label>
			<input type="text" name="surname" class="form-control" placeholder="Surname" /> <br />
			
			<label for="email">Email:</label>
			<input type="email" name="email" class="form-control" placeholder="Email" /> <br />
			
			<label for="phone">Phone Number:</label>
			<input type="number" name="phone" class="form-control" placeholder="Phone" /> <br />
			
			<label for="password">Password:</label>
			<input type="password" name="password" class="form-control" placeholder="Password" /> <br />
			
			<div  class="checkbox col-md-4 col-sm-12">
				<label><input type="checkbox" name="agreeCheckbox"> I agree to the Terms and Data Policies</label>
			</div>
			
			<div class="panel-group col-md-8 col-sm-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" href="#collapsibleArea">View Terms and Data Policies</a>
						</h4>
					</div>
					<div id="collapsibleArea" class="panel-collapse collapse">
						<div class="panel-body">
							1) By registering with geoshare you agree to let your details be viewable and processed safely by the professional developers team of geoshare. <br>
							2) By registering with us you consent to let your name be viewable to other users of the application. <br>
							3) By registering with us you agree to let borrowers of your item(s) to view your contact details. <br>
							4) You must not commit any frauds, otherwise your account will be terminated and you may be charged with fraud and therefore face the consequences.
						</div>
					</div>
				</div>
			</div>
			
			<input type="submit" name="submit" value="Register" class="btn btn-primary form-control" /><br />
		</div>
		
		<input name="accessType" value="register" style="visibility: hidden; display: none;" />
		
		</form>
		
		<center><button onclick="location.href='login.php'" type="button" class="btn btn-link btn-lg">Login</button></center>
	</div>

	</body>
</html>